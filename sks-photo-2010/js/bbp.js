jQuery(document).ready(function($){


 // $(".ngg-gallery-thumbnail a[title$='(video)']").each(function (arr){
 //     $(this).attr("rel","shadowbox;width=640;height=480");
 //     $(this).attr("href",$(this).children("img").attr("title"));
 //});
 
 var bbpHome = $('#site-info .site-url').text();
 
 $(".ngg-gallery-thumbnail a[title$='(video)']").each(function (arr){
     var thereltag = $(this).attr("rel").substr(0,22);
     $(this).attr("rel", thereltag + ";width=640;height=480");
     $(this).attr("href",$(this).children("img").attr("title"));
     var thesrctag = $(this).children("img").attr("src");
     $(this).children("img").attr("src", bbpHome + "wp-content/themes/bb-photo-2010/hover.php?path=" + thesrctag);
 })
 
  beastBox.addWrapper('#content');
  beastBox.addControls();
 
 
 $(".ngg-gallery-thumbnail .beastbox").click(function(e) {
     e.preventDefault();
     beastBox.updatePhoto(this);        
 });
	
});

var beastBox=(function() {

    //make settings
    var wrapSel = '#content';
    var galleryClass = 'photo-display';
    var gallerySel = '.'+galleryClass;
    var thumbWrapClass = 'gallery-page';
    var thumbWrapSel = '.'+thumbWrapClass;
    
    //controlSel is assumed to be class
    var controlClass = 'controls';
    var controlSel = '.'+controlClass;
    
    var getGalleryTotal = function() {
        var tot = $('.ngg-gallery-thumbnail-box').length;
       //console.log(tot);
        return tot;
    };    
        
    var getCurrentPos = function(curr) {
        //yeesh
        return $(curr).parent().parent('.ngg-gallery-thumbnail-box').attr('id').split('-')[2];
    };
    
    var hideThumbnails = function() {
        $(thumbWrapSel, wrapSel).hide();
    };
    
    var hideCurrentImage = function() {
        $(gallerySel, wrapSel).hide();
        //console.log('hiding...');
        //swap switch action class, reapply
    };
    
    var showThumbnails = function() {
        hideCurrentImage();
        $(thumbWrapSel, wrapSel).show();
    };
    
    var showGallery = function() {
        hideThumbnails();
        $(gallerySel, wrapSel).show();
        updateControls();
    }
    
    var updateTotal = function() {
        var tot = getGalleryTotal();
        $(controlSel+' .total', wrapSel).html(tot);
    };
    
    var updateCurrentPosition = function(curr) {
        var currentPos = getCurrentPos(curr);
        $(controlSel+' .curr', wrapSel).html(currentPos);
    };
    
    var resetCurrentPosition = function() {
        $(controlSel+' .curr', wrapSel).html('1');
    };
    
    var setSwitchAction = function() {
        if ($(gallerySel, wrapSel).is('visible')) {
            console.log(gallerySel + 'is visible');
           $(controlSel+' .switch', wrapSel).unbind('click').bind('click', function(){
           //will show last current image, unless fwd/back'd - behavior here should be?
           //not currently working - what is state when addControls fires?
               showGallery();
            }); 
        } else { 
            //console.log(gallerySel + 'is invisible');
            $(controlSel+' .switch', wrapSel).unbind('click').bind('click', function(){
            //will show last current image, unless fwd/back'd - behavior here should be?
                showThumbnails();
                resetCurrentPosition();
             });      
        }
    };

    var addWrapper = function(sel) {
        //use this sel to update config?
        $(sel).prepend('<div class="'+galleryClass+'"></div>');
        addControls();
    };
    
    var updatePhoto = function(curr) {
        //expects DOM element with href to displayed image
        var src = $(curr).attr("href");
        $(gallerySel, wrapSel).html('');
        $(gallerySel, wrapSel).append('<img src="'+src+'"/>');
        showGallery();
        updateCurrentPosition(curr); 
        
        
              
    };  
    
    var addControls = function() {
        var controlMarkup = '<div class="'+controlClass+'"><span class="curr">1</span><span class="sep">/</span><span class="total">X</span>';
        controlMarkup += '<span class="switch"> -x- </span><span class="back">Back</span><span class="forward">Forward</span></div>';
        if ($(controlSel, wrapSel).get(0) == null) {
            $(wrapSel).prepend(controlMarkup);
            updateControls();
        } else { 
        //  console.log('...controls already present');
            updateControls();
        };
    };
    
    var updateControls = function() {
        updateTotal();
        setSwitchAction();
     // console.log('...updating');
    };

    return {
        'addWrapper':addWrapper,
        'updatePhoto':updatePhoto,
        'hideThumbs':hideThumbnails,
        'addControls':addControls
    }
}());