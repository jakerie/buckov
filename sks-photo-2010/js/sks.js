jQuery(document).ready(function($){

   /*    $('a.beastbox img').asynchImageLoader({
           timeout: 2000,
           effect: 'fadeIn',
           speed: 2000
       });
  */
               

 // $(".ngg-gallery-thumbnail a[title$='(video)']").each(function (arr){
 //     $(this).attr("rel","shadowbox;width=640;height=480");
 //     $(this).attr("href",$(this).children("img").attr("title"));
 //});
 
  beastBox.addWrapper('#content');
  beastBox.addControls();
   
 $(".ngg-gallery-thumbnail .beastbox").click(function(e) {
     e.preventDefault();
     beastBox.initGallery($(this).parent().parent('.ngg-gallery-thumbnail-box'));        
 });
 
 if ( $("body.home").get(0) != null && $('.ngg-gallery-thumbnail-box').get(0) != null ) {
     beastBox.initHomePageImage();     
 }

if ( $("body.page .about-page").get(0) != null && $('.ngg-gallery-thumbnail-box').get(0) != null ) {
    beastBox.initHomePageImage();     
}	
	
});

var beastBox=(function() {

    //make settings
    var wrapSel = '#content';
    var galleryClass = 'photo-display';
    var gallerySel = '.'+galleryClass;
    var thumbWrapClass = 'gallery-page';
    var thumbWrapSel = '.'+thumbWrapClass;
    
    //controlSel is assumed to be class
    var controlClass = 'controls';
    var controlSel = '.'+controlClass;
    var gallerySetArr = $.makeArray($('.ngg-gallery-thumbnail-box'));
    var galleryCurrentIndex = 0; //is overwritten at new initialization of gallery

    var getGalleryTotal = function() {
        var tot = $('.ngg-gallery-thumbnail-box').length;
        return tot;
    };    
        
    var getCurrentPos = function(arr) {
      
      var htmlElement = arr[0]==undefined?arr:arr[0];
      var currPos = $.inArray(htmlElement, gallerySetArr);
      return currPos+1;
      
    };

    var hideThumbnails = function() {
        $(thumbWrapSel, wrapSel).hide(); 
    //    setSwitchAction('grid');
    };
    
    var hideCurrentImage = function() {
        $(gallerySel, wrapSel).hide();
        setSwitchAction('grid');
    };
    
    var showThumbnails = function() {
        hideCurrentImage();
        $(thumbWrapSel, wrapSel).show();
    };
    
    var showGallery = function() {
        hideThumbnails();
        $(gallerySel, wrapSel).show();      
        updateControls();
        setSwitchAction('single');
    }
    
    var updateTotal = function() {
        var tot = getGalleryTotal();
        $(controlSel+' .total', wrapSel).html(tot);
    };
    
    var updateCurrentPosition = function(curr) {
        var currentPos = getCurrentPos(curr);
        $(controlSel+' .curr', wrapSel).html(currentPos);
    };
    
    var resetCurrentPosition = function() {
        $(controlSel+' .curr', wrapSel).html('1');
    };
    
    var setSwitchAction = function(vers) {
        var view=vers;
        if (view == 'grid') {
           $(controlSel+' .prev').css('visibility', 'hidden');
           $(controlSel+' .next').css('visibility', 'hidden');
   
           $(controlSel+' .switch').removeClass('grid-view').addClass('single-view');
           
           $(controlSel+' .switch', wrapSel).unbind('click').bind('click', function(){
               //will show last current image, unless fwd/back'd - behavior here should be?
               updatePhoto(gallerySetArr[galleryCurrentIndex]);
            }); 
        } else { 

       $(controlSel+' .prev').css('visibility', 'visible');
       $(controlSel+' .next').css('visibility', 'visible');
        $(controlSel+' .switch').removeClass('single-view').addClass('grid-view');
        $(controlSel+' .switch', wrapSel).unbind('click').bind('click', function(){
               showThumbnails();
               resetCurrentPosition();
         });      
        }
    };
      
    var setNextAction = function(curr) {
        var nextIndex = getIndexPos(curr)+1;
        var arrayTotal=getGalleryTotal()-1;
        if (nextIndex > arrayTotal) { nextIndex = 0 };    
   
        $(controlSel+' .next').unbind('click').bind('click', function() {
             updatePhoto(gallerySetArr[nextIndex]);
        });
        
    };
    
    var setPrevAction = function(curr) {
        var prevIndex = getIndexPos(curr)-1;
        if (prevIndex < 0) { prevIndex = getGalleryTotal()-1; }

        $(controlSel+' .prev').unbind('click').bind('click', function() {
             updatePhoto(gallerySetArr[prevIndex]);
        });
            
    };

    var addWrapper = function(sel) {
        //use this sel to update config?
        $(sel).prepend('<div class="'+galleryClass+'"></div>');
        addControls();
    };
    
    
    var checkVideo = function(link) {
     //dependent on title tag of surrounding anchor containing string "(video)"
     var isVideo = false;
     var titleAttr = $(link).find("a.beastbox").attr("title");
    
    //console.log(titleAttr);
    
    if (titleAttr != undefined) {
         if (titleAttr.toLowerCase().indexOf("(video)") >= 0) {
                isVideo = true;        
         }
     }
      return isVideo;    
    }
    
    var updatePhoto = function(arr) {
        //expects DOM element with href to displayed image

        var src = $(arr).find("a.beastbox").attr("href"),
            caption = $(arr).find("a.beastbox").attr("title"),   
            spannedCap = "<span class='caption'>" + caption + "</span>";  
        //console.log(src);
        
        $(gallerySel, wrapSel).html('');
        //.append('<img src="'+src+'"/>');
        
       //  if($(arr).find("a[title$='(video)']")){console.log('video')};
             var isVideo = checkVideo(arr);
        
        if (isVideo) {
            
            src = $(arr).find("a.beastbox img").attr("alt");
            
         //   console.log(src);
        
            $("<iframe />").attr({
                	'id' : 'beastbox-img',
                	'src' : src,
                	'alt' : caption,
                	'width' : '655',
                	'height' : '434'
                }).appendTo( $(gallerySel, wrapSel) );
        
        } else {            
            $("<img />").attr({
            	'id' : 'beastbox-img',
            	'src' : src,
            	'alt' : caption
            }).appendTo( $(gallerySel, wrapSel) );
        }   
        
        $(gallerySel, wrapSel).append( spannedCap );
        
         
        //showGallery does the sets the view switch
        showGallery();

        updateCurrentPosition(arr); 
        
        setNextAction(arr);
        setPrevAction(arr);     
              
    };  
        
    var getIndexPos = function(curr) {
        //refactor with $.inArray?
        //dependent on nextgen markup
        var initPos, indexArrPos;
        var htmlElement = curr[0]==undefined?curr:curr[0];
        
        initPos = $.inArray(htmlElement, gallerySetArr);
        indexArrPos = initPos;
        galleryCurrentIndex=indexArrPos;

        return indexArrPos;
    };
    
    var initGallery = function(curr) {

        var initIndex = getIndexPos(curr);

        updatePhoto(gallerySetArr[initIndex]);
                
     //   setSwitchAction('single');
    };
    
    var initHomePageImage = function() {
        hideControls();
        cleanupHome();
        updatePhoto(gallerySetArr[0]);
    }
        
    var addControls = function() {
        var controlMarkup = '<div class="'+controlClass+'"><span class="curr">1</span><span class="sep">/</span><span class="total">X</span>';
        controlMarkup += '<span class="prev"><span class="text">previous</span></span><span class="switch grid-view"><span class="view">-x-</span></span><span class="next"><span class="text">next</span></span></div>';
        if ($(controlSel, wrapSel).get(0) == null) {
            $(wrapSel).prepend(controlMarkup);
            updateControls();
            setSwitchAction('grid');
        } else { 
            updateControls();
        };
    };
    
    var cleanupHome = function() {
        $('.entry-meta').html('');
        $('.entry-title').html('');
        $('.ngg-galleryoverview').html('');
    };
    
    var hideControls = function() {
        //remove controls from page
        $(controlSel).html('');
    };
    
    var updateControls = function() {
        updateTotal();
    };

    return {
        'addWrapper':addWrapper,
        'initGallery':initGallery,
        'hideThumbs':hideThumbnails,
        'initHomePageImage':initHomePageImage,
        'addControls':addControls
    }
}());
