<!DOCTYPE html>
<html>
<head><title>test classes</title>
<style type="text/css">
	.inline-left {
	    float: left;
	    display: inline;
	    padding: 12px;
	}
	.inline-right {
	    float: right;
	    display:inline;
	    padding: 12px;
	}
	.block-left {
	    display: block;
        margin-left: 0;
        margin-right: auto;
        padding: 12px 0 12px 0;
	}
	.block-right {
        display: block;
        margin-left: auto;
        margin-right: 0;
        padding: 12px 0 12px 0;
	}
	
	.block-center {
	    clear: both;
	    display: block;
	    margin-left: auto;
	    margin-right: auto;
	    padding: 12px 0 12px 0;
	}
	
</style>

</head>
<body>


<div style="width:500px;">

<img src="images/headers/berries-thumbnail.jpg" class="inline-left" alt="class: inline-left" title="class: inline-left"/>Here is a bit of text wrapping around an image. This can be on the left, or the right. Wrapping text around an image in the center doesn't really work, because text does not flow across an element in the page naturally. All images have a padding around them of 12 pixels, though this can be altered if desired. <img src="images/headers/fern-thumbnail.jpg" class="inline-right" alt="class: inline-right" title="class: inline-right"/> Since images take up discrete space in the page, you do not need to specify their height and width. However, if using these classes to separate text from the main body, you will need to specify a height and a width for the container you are adding the class to. <div class="test inline-right" style="width:150px;height:200px;"> This div has a height of 200px and a width of 150px. It is set to float to the right of the main body of text that it is sitting in by adding the class "inline-right" to it, and then specifying the height and width with a style attribute.</div> That will allow the text to sit off from the main body and can be styled in any way using CSS. The other style of class available to you are block-right, block-center and block-left. These classes work in the same manner with respect to the images, but the text will not wrap around them.<img src="images/headers/fern-thumbnail.jpg" class="block-right" alt="class: block-right" title="class: block-right"/> Notice how an image with block-left sits within a column set by the floated element to its right. These classes will allow you to create more complex layouts within your posts if you desire. Additionally, when the specified height of another div or image, such as the one immediately to the right here, runs out of "height", the text will begin wrapping underneath that element. <img src="images/headers/berries-thumbnail.jpg" class="block-center" alt="class: block-center" title="class: block-center"/> Additionally, block-center images will break down below any previously floated right or left content, and display on its own line with padding of 12px on the top and bottom, so if you had put block-center where the block-right image is above within the smaller column on the left, all of the text would show below the centered image, below the div with the text on the right. <img src="images/headers/fern-thumbnail.jpg" class="block-right" alt="class: block-right" title="class: block-right"/> Images that use block-right or block-left will sit on their own line and float to the side of their own line, without text wrapping around them. <img src="images/headers/berries-thumbnail.jpg" class="block-left" alt="class: block-left" title="class: block-left"/> This is how you would use any of these classes. They will need to be added to your images after you insert them into a post using the WP-uploader.
<br/><br/>
Iframe with inline-right. I'm not sure how classes work on iframes, but here I am, giving a shot.
<iframe class="inline-right" src="http://player.vimeo.com/video/22875944" width="400" height="225" frameborder="0"></iframe> This video should float to the right of the text that is wrapping to its left. It seems like it is working, but I need to add more text to see that it is wrapping properly. So that's cool, it seems to work. The classes will work when you place them directly on the iframe. That said, you're taking the tag of text that Vimeo provides. <iframe class="inline-left" src="http://player.vimeo.com/video/23428725?title=0&amp;byline=0&amp;portrait=0" width="400" height="225" frameborder="0"></iframe><p><a href="http://vimeo.com/23428725">Coffee Time</a> from <a href="http://vimeo.com/user4316543">wan-tzu</a> on <a href="http://vimeo.com">Vimeo</a>.</p> You'll obviously have to wrap it in a container, like a div with the inline class on it, or the display of the &lt; p &gt; tag will be unpredictable. However, if you place it within its own container, such as this:
<br/>
The &lt; p &gt; tag will display underneath the video, and the text will wrap around the video as well as this tag, as you would expect. So that is how these classes work with videos. <div class="video inline-left">
<iframe src="http://player.vimeo.com/video/23428725?title=0&amp;byline=0&amp;portrait=0" width="400" height="225" frameborder="0"></iframe><p><a href="http://vimeo.com/23428725">Coffee Time</a> from <a href="http://vimeo.com/user4316543">wan-tzu</a> on <a href="http://vimeo.com">Vimeo</a>.</p>
</div>
In order to have this work with videos, you will have to grab the embed code - you won't just be able to put the short vimeo URL into your post, since that conversion happens by the Word Press engine, you won't be able to put classes onto the output of that url. That said, this example shows, you can wrap the vimeo URL in a div with one of these classes, and as long as that conversion of the URL happens within that div with that class, everything should display as you expect it too.


</div>


</body>
</html>