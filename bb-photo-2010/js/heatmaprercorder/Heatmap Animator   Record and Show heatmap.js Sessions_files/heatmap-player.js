;(function(global){

    var player = (function(){

        var sessions = [],
            heatmap = null,
            sessionData = { data: [] },
            data = [],
            last = 0,
            recording = false,
            playing = false,
            animation = null,
            activeID = null,
            snapshotID = null,
            animationEl = null,
            $ = function(id){
                return document.getElementById(id);
            },
            initialize = function(){
                loadSessions();
                $('record').onclick = function(){
                    var text;
                    if(recording){
                        stopSession();
                        recording = false;
                        text = '';
                    }else{
                        startSession();
                        if(playing){
                            playPause();
                        }
                        recording = true;
                        text = 'rec';
                    }
                    this.className = text;
                };
                $('playpause').onclick = function(){
                    playPause();
                    if(!playing){
                        this.innerHTML = '&#x25b6;';
                    }else{
                        this.innerHTML = '❚❚';
                    }
                };
                $('fps').onchange = function(){
                    setFrameRate(+this.value);
                };
                $('loading').style.display = 'none';
            },
            startSession = function(){
                var el = $('surface');
                if(animationEl){
                    el.removeChild(animationEl);
                    animationEl = null;
                }
                heatmap = h337.create({
                    element: el,
                    "radius":35, 
                    "visible":true
                });
                
                (function(){
                    var mouseover = false,
                        mousemove = false,
                        active = false,
                        idleTimer = null,
                        lastCoords = [],
                        activate = function(){
                            active = true;
                        },
                        simulateEv = function(){
			    heatmap.store.addDataPoint(lastCoords[0], lastCoords[1]);
			},
			antiIdle = function(){
			    if(mouseover && !mousemove && lastCoords && !idleTimer){
			        idleTimer = setInterval(simulateEv, 1000);			
			    }
			},
                        snap = function(){
                            data.push(heatmap.store.exportDataSet());
                        };
                    data = [];

                    el.onmouseout = function(){
                        mouseover = false;
                        if(idleTimer){
                            clearInterval(idleTimer);
                            idleTimer = null;
                        }
                    };
                    (function(fn){
                        setInterval(fn, 1000);
                    })(antiIdle);
        
                    el.onmousemove = el.onclick = function(ev){
                        mouseover = true;
                        mousemove = true;
                        if(active){
                            if(idleTimer){
                                clearInterval(idleTimer);
                                idleTimer = null;
                            }
                            var pos = h337.util.mousePosition(ev);
                            if(pos){
                                heatmap.store.addDataPoint(pos[0], pos[1]);
                                lastCoords = [pos[0], pos[1]];
                            }
                            active = false;
                        }
                        mousemove = false;
                    };
                    el["ontouchmove"] = function(ev){
                        var touch = ev.touches[0],
                            simulatedEvent = document.createEvent("MouseEvent");
                        simulatedEvent.initMouseEvent("mousemove", true, true, window, 1,  touch.screenX, touch.screenY, touch.clientX, touch.clientY, false,false, false, false, 0, null); 

                        touch.target.dispatchEvent(simulatedEvent);
                        ev.preventDefault();
                        
                    };

                    (function(fn, fs){
                        activeID = setInterval(fn, 50);
                        snapshotID = setInterval(fs, 1500);

                    })(activate, snap);

                })();
            },
            stopSession = function(){
                heatmap.toggleDisplay();
                var el = $('surface');
                el.onmousemove = el.onclick = el.onmouseout = null;
                clearInterval(activeID);
                clearInterval(snapshotID);
                addSession(data);
                //console.log(JSON.stringify(data));
            },
            addSession = function(data){
                sessions.push(data);
                loadSessions();
            },
            loadSessions = function(){
                last = 0;
                var el = $('session-container'),
                    html = '',
                    ul = document.createElement('ul'),
                    sesslen = sessions.length;
                // retrieve locally saved sessions + 3 test sessions
                // TODO: localStorage?
                
                for(var i = 0; i < sesslen; i++){
                    html += ('<li onclick="HeatmapPlayer.loadSession('+i+');">Session '+i+'</li>');
                }
                
                el.innerHTML = '<ul>'+html+'</ul>';
                sesslen > 0 && loadSession(sesslen-1);
                //
                // generate html list of sessions containing session date
                // add onclick listeners for load session / delete session
            },
            loadSession = function(id){
                $('loading').className = 'loading';
                // if clicked on specific session
                var el = $('surface');
                    last = 0;
                if(animationEl){
                    el.removeChild(animationEl);
                    animationEl = null;
                }
                data = sessions[id];
                // - generate heatmapanimation
                animation = h337.createAnimation({
                    element: el,
                    radius: 35,
                    visible: false,
                    data: data,
                    framerate: 1,
                    onReady: function(){
                        //console.log('remove loading');
                        $('loading').className = '';
                    },
                    callback: function(index){
                        setSelected(index);    
                    }
                });
                animationEl = animation.get('animationEl');
                el.appendChild(animationEl); 
                // - generate timeline with clickable snapshots
                var html = '',
                    dlen = data.length,
                    offset = (600/(dlen))-20;

                for(var i = 0; i < dlen; i++){
                    html += ('<li onclick="HeatmapPlayer.setFrame('+i+')" id="frame-'+i+'">Frame #'+i+'</li>');
                }
                $('timeline').innerHTML = '<ul>'+html+'</ul>';
            },
            setSelected = function(index){
                
                $('frame-'+last).className = '';
                last = index;
                $('frame-'+last).className = 'selected';
             },
            playPause = function(){
                if(!playing){
                    animation.start();
                    playing = true;
                }else{
                    animation.stop();
                    playing = false;
                }
            },
            setFrameRate = function(rate){
                animation.set('framerate', rate);
            };

        return {
            init: function(){
                var script = document.createElement("script");
                script.src = "data.js";
                document.body.appendChild(script);
            },
            loadSession: function(id){
                loadSession(id);
            },
            setFrame: function(index){
                if(playing){
                    $('playpause').innerHTML = '&#x25b6;';
                    playPause();
                }
                animation.setFrame(index);
                setSelected(index);
            },
            getData: function(data){
                sessions.push(data);
                initialize();
            }
        };
    })();


    global.HeatmapPlayer = player;
})(this);
