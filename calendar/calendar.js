define('calendar', function(require, exports, module) {
    var _this,
    	_params,
    	_calendarContainer = document.getElementById("calendar"),
    	_dayContainer,
    	_months = ["January","February","March","April","May","June","July","August","September","October","November","December"],
    	_days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    	_hoursMilitary = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300],
    	_hoursStandard = [12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
    	_itemTemplate = document.createElement('div'),
    	_displayArray = [],
    	_conflictArray = [],
    	_conflictStart,
    	_conflictEnd,
    	_eventData,
    	_calendarConfig;    

    	function init() {
	    	_this.getCalendarConfig();
    	};
    
    	function Calendar(params) {
	    	_this = this;
	    	if (params.itemTemplate) {
		    	_itemTemplate = params.itemTemplate.cloneNode(true);
		    	_itemTemplate.removeAttribute("id");
	    	}
	    	init();
    	};
    	
    	function within(x, min, max) {
		  return x >= min && x <= max;
    	};
	    
	    Calendar.prototype = {
		  	getCalendarConfig: function() {
			  	//year/month
			  	//year/month/day
				var cc = require('calendarConfig');
				_calendarConfig = new cc.CalendarConfig({
					request: "calendarConfig.js",
					testMode: true
				})
				_this.renderCalendar(_calendarConfig);
				
		  	}, 
		  	renderCalendar: function(cfg) {
		  		var thisDate;

		  		for (var y=0, l = cfg.years.length; y < l; y+=1) {
			  		var year = cfg.years[y],
			  			yearDisplay = year.value;
			  		
			  		thisDate = yearDisplay.toString();
			  		
			  		for (var m=0, l=year.months.length; m<l; m+=1) {
				  		var month = year.months[m],
				  			monthDisplay = _months[month.value];
				  		
				  		thisDate += month.value;
				  		for (var d=0, l=month.days.length; d<l; d+=1) {
					  		var day = month.days[d],
					  			dayDisplay = day.value,
					  		    dayStart = Number(day.start.value),
					  		    dayEnd = Number(day.end.value),
					  		    hours = dayEnd/60;
					  		 
					  		thisDate += dayDisplay;  
				  		    var startTimeIndex = _hoursMilitary.indexOf(day.start.time),
				  		    	endTimeIndex = _hoursMilitary.indexOf(day.end.time),
				  		    	timeCol = document.createElement('div'),
				  		    	timeIndex = "", 
				  		    	i = startTimeIndex + hours;
				  		   
					  		    timeCol.setAttribute("id", thisDate +"-time");
					  		    timeCol.setAttribute("class", "time-col");
					  		    	
					  		    for (var i = startTimeIndex; i <= endTimeIndex; i+=1){
					  		    	var amPm = i >= 12 ? 'pm' : 'am',
					  		    	    hour = _hoursStandard[i],
					  		    	    isLast = i === endTimeIndex;
					  		    	 
					  	  		    timeIndex = timeIndex+"<div class='hr'><span class='hour'>"+ hour +":00</span><span class='period'>"+ amPm +"</span>"
					  	  		    if (!isLast) {
					  	  		    	timeIndex +="<p class='half'>"+ hour +":30</p>";
					  	  		    }
					  	  		    
					  	  		    timeIndex += "</div>";
					  		    }
					  	}
				  		
			  		}
			  		
		  		}
		  		timeCol.innerHTML = timeIndex;
				_calendarContainer.appendChild(timeCol);
		  		_this.loadEventData(thisDate);
			  	
		  	},
		  	loadEventData: function(date) {
		  		var reqUrl = date + ".json";

		  		var cd = require('calendarData');
	
		  		var data = new cd.CalendarData({
		  			request: reqUrl,
			  		testMode: true
		  		});
		  		
		  		_this.renderEventData(data);
		  		
		  		
		  	},
		  	renderEventData: function(data) {
			  	switch (data.type) {
				  	case "day":
				  		_this.layOutDay(data.day[0].events, data.day[0].value);
				  		break;
				  	case "month":
				  		//do something
				  		break;
				  	case "year": 
				  		//do something
				  		break;  	
			  	}
		  	},
		  	layOutDay: function(events, day) {
		  		//assumption: events are coming in order
		  		var dayId = day || Math.random();
		  		
		  		if (_dayContainer) {
			  		_dayContainer.innerHTML = "";
			  		_conflictArray = [];
		  		} else {
		  			_dayContainer = document.createElement('div');
		  		}
		  		
		  		
		  		
		  		_dayContainer.setAttribute("id", dayId + '-day');
		  		_dayContainer.setAttribute("class", "events");  
		  		
		  		if (events.sort) {
		  			events.sort(function(x,y){return x.start - y.start});
		  		}
		  		var copy = events;	
		  		
		  		//check for conflicts
		  		for (var e=0, l=events.length; e<l; e+=1) {
		  			var newEventStart = events[e].start,
		  			    newEventEnd = events[e].end;
		  			    
			  	  	for (c=0, l=copy.length; c<l; c+=1) {
				  	 	var potentialConflictStart = copy[c].start, 	
				  	 	    potentialConflictEnd = copy[c].end;
		
				  	 	    if (events[e] !== copy[c]) {
					  	 	    if (within(newEventStart, potentialConflictStart, potentialConflictEnd) || within(newEventEnd, potentialConflictStart, potentialConflictEnd)) {
					  	 	       //if this conflict is already in the array, don't add again
					  	 	       //sometimes you get a conflict from both ends--start conflicts with one event, end conflicts with another
					  	 	       if (_conflictArray.indexOf(copy[e]) === -1){ 
						  	 	   		_conflictArray.unshift(copy[e]);
						  	       }
					  	 	   } 
			    	 	    }
				  	 } 
		  		}
	
		  		for (var e=0, l=events.length; e<l; e+=1) {
		  			//render events without conflicts
			  		if (_conflictArray.indexOf(events[e]) === -1) {
				  		var _displayArray = events,
				  			thisEvent = _this.renderEvent(_displayArray[e]);
						  	_dayContainer.appendChild(thisEvent);			 
					  	}
				}
				
				_this.renderConflicts();
	
		  		_calendarContainer.appendChild(_dayContainer);

		  	},
		  	renderConflicts: function() {
		  		var temporaryArray;
		  				  		
		  		//debugger;	
		  		if (_conflictArray.length > 0) {
				    var currentConflict = {};
					    currentConflict.conflicts = [];
					    currentConflict.start = _conflictArray[0].start;
					    currentConflict.end = _conflictArray[0].end;
					    
					    //add conflict to currentConflicts
					    currentConflict.conflicts.unshift(_conflictArray[0]);
					    
					    //remove comparator from _conflictArray
					    _conflictArray.splice(0, 1);
					    
					    //use temporary array to keep track of what's been evaluated
					    temporaryArray = _conflictArray.slice();
					    
				    for (var e=0, l = _conflictArray.length; e < l; e += 1) {
					    //console.log(_conflictArray[e]);
					      if (within(_conflictArray[e].start, currentConflict.start, currentConflict.end) || within(_conflictArray[e].end, currentConflict.start, currentConflict.end)) {
					  	 	 //set proper bounds for conflict
					  	 	 currentConflict.start = Math.min(_conflictArray[e].start, currentConflict.start);
					  	 	 currentConflict.end = Math.max(_conflictArray[e].end, currentConflict.end);
				  	 	  	 
				  	 	  	 //this belongs in currentConflicts
				  	 	  	 currentConflict.conflicts.unshift(_conflictArray[e]);
				  	 	  	 
				  	 	  	 //remove from _conflictArray
				  	 	  	 temporaryArray.splice(temporaryArray.indexOf(_conflictArray[e]), 1);		
				  	 	  } 
			  	 	 			  	 	  
					    }
					  
					  //displayConflicts
					  _this.displayConflicts(currentConflict);
					  
					  //if there are more conflicts, process again
					  if (temporaryArray.length > 0) {
					  	  _conflictArray = temporaryArray;
						  _this.renderConflicts();
					  }  
					  
			    }
		  	},
		  	displayConflicts: function(conflict) {
			  var events = conflict.conflicts,
			  	  numOfConflicts = events.length;
			  	  thisConflict = document.createElement('div');
			  	  thisConflict.setAttribute("class", "conflict-box");
			  	  thisConflict.style.top = conflict.start + "px";
			  	  thisConflict.style.height = (conflict.end - conflict.start) + "px";
			  	 //thisConflict.style.backgroundColor = "white";

			  	  for (var e=0, l=events.length; e<l; e+=1) { 
			  	  	  //adjust event by conflict box bounds
			  	  	  events[e].start = events[e].start - conflict.start;
			  	  	  events[e].end = events[e].end - conflict.start;
			  	  
				      var eventObj = _this.renderEvent(events[e]);
				      	  eventObj.style.width = (99/numOfConflicts) + "%";
				      	  //eventObj.style.cssFloat = e%2?"right":"left";
				      	  eventObj.style.left = (100/numOfConflicts) * e + "%";
				      	  
				      	  thisConflict.appendChild(eventObj);
			  	  }
			  	  
			  	  _dayContainer.appendChild(thisConflict);
			  	  	
		  	},
		  	renderEvent: function(event) {
			  	var thisEvent = _itemTemplate.cloneNode(true),
			  		thisEventStart = event.start,
			  		eventLength = event.end - event.start;
			  		
			  		thisEvent.style.top = event.start + "px";
			  		thisEvent.style.height = eventLength + "px";
			  		
			  		return thisEvent;
		  	},
		  	renderErrorState: function(e) {
		  		console.log(e);
			  	//do something
		  	}
	    }; 

	    exports.Calendar = Calendar;
	     
    });