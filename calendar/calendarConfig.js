define('calendarConfig', function(require, exports, module) {

//originally setup using json files, but chrome's local file policy
//prevented it from working 


	var _params,
		_sampleConfig = {
			 "years": [{
				 "value": 2013,
				 "months": [{
				 		"value": 4,
				 		"days": [{
					 		"value": 13,
					 		"start": {
						 		"value": 0,
						 		"time": 900
					 		},
					 		"end": {
						 		"value": 720,
						 		"time": 2100
					 		}
				 		}]
				 	}]
				 }]	
			 }	

function init() {
	_this.getCalendarConfig();	
};

function CalendarConfig(params) {
	_params = params;
	if (_params.testMode) return _sampleConfig;
	init();
};

CalendarConfig.prototype = {

	getCalendarData: function() {
		var request = _params.request;
	/*
	  	$.ajax({
			url: request,
			dataType: "json",
		}).success(function(data) {
			//data normalizer
			return data;
		}).error(function(e) {
			_this.renderErrorState(e);
		}); 
	*/			
	},
	renderDataError: function(e) {
		//do something	
	}
};

exports.CalendarConfig = CalendarConfig;

});