define('calendarData', function(require, exports, module) {

//originally setup using json files, but chrome's local file policy
//prevented it from working 


//initial data

[ ]
	var _params,
		_dataExample = {
			"type": "day",
			"day": [{
				"events": [{
					"start": 30, 
					"end": 150
					},{
					"start": 540, 
					"end": 600
					},{
					"start": 560, 
					"end": 620
					},{
					"start": 610, 
					"end": 670
					}]
				}]
		};
	/*	_dataExample = {
			"type": "day",
			 "day": [{
				 "value": 2013413,
				 "events": [{
				 	 "start": 0,
				 	 "end": 60
				 		},{
					 "start": 30,
					 "end": 90
				 		},{
					 "start": 200,
					 "end": 300
				 		},{
					 "start":175,
					 "end":375
				 		},{
					 "start": 275,
					 "end":350
				 		},{
					 "start": 325,
					 "end": 375
				 		},{
					 "start": 515,
					 "end": 575
			 		}]
			 }]*/

function init() {
	_this.getCalendarData();	
};

function CalendarData(params) {
	_params = params;
	if (_params.testMode) return _dataExample;
	init();
};

CalendarData.prototype = {

	getCalendarData: function() {
		var request = _params.request;
	/*
	  	$.ajax({
			url: request,
			dataType: "json",
		}).success(function(data) {
			//data normalizer
			return data;
		}).error(function(e) {
			_this.renderErrorState(e);
		}); 
	*/			
	},
	renderDataError: function(e) {
		//do something	
	}
};

exports.CalendarData = CalendarData;

});