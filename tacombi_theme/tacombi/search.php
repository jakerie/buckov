<?php
/**
 * @package WordPress
 * @subpackage Tacombi_Theme
 */

get_header(); ?>

<div id="tacombi-home" class="searchPage">
	<div id="intro">
		<p> 
<?php if (have_posts()) : ?>
        You searched for "<span class="searchResult"><?php the_search_query() ?></span>".
<?php else : ?>
        We're sorry. It looks like we don't have anything for "<span class="searchResult"><?php the_search_query() ?></span>." Why not try again?
<?php endif; ?>
		</p>	
	</div>
	
	<?php include (TEMPLATEPATH . '/third-column.php'); ?>
	
	<div id="content" class="widecolumn searchPage" role="main">

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?>>
				<div class="archive_list"><span id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a> </span> by <span class="author"><?php the_author() ?></span> on <?php the_time('j F Y') ?>
			</div>
<!--
				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
-->
			</div>

		<?php endwhile; ?>

		<div class="navigation">
	    	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
		</div>

	<?php else : ?>

		<?php get_search_form(); ?>

	<?php endif; ?>

	</div>

<?php get_footer(); ?>
