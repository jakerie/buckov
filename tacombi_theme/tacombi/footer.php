<?php
/**
 * @package WordPress
 * @subpackage Tacombi_Theme
 */
?>

<hr />

</div>

<?php /* "Just what do you think you're doing Dave?" */ ?>

		<?php wp_footer(); ?>
		
		<div id="footer" role="contentinfo">
		<!-- If you'd like to support WordPress, having the "powered by" link somewhere on your blog is the best way; it's our only promotion or advertising. -->
			<!--
			<p>
				<?php bloginfo('name'); ?> is proudly powered by
				<a href="http://wordpress.org/">WordPress</a>
				<br /><a href="<?php bloginfo('rss2_url'); ?>">Entries (RSS)</a>
				and <a href="<?php bloginfo('comments_rss2_url'); ?>">Comments (RSS)</a>.
				 <?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. 
			</p>
		-->
		<div class="taco-subnav">
		
		      <ul class="nav">
		            <li class="about <?php 
		                            if ($isAbout)  
		                            { 
		                            echo " current";
		                            }?>"><a href="<?php bloginfo('url'); ?>/about" class="about">About</a><span class="sep"> / </span></li>
		                        <!--   <li><span class="sep">/</span></li>-->
		          <li class="food <?php 
		                          if ($isFood)  
		                          { 
		                          echo " current";
		                          }?>"><a href="<?php bloginfo('url'); ?>/food" class="food">Food</a><span class="sep"> / </span></li>
		                         <!--   <li><span class="sep">/</span></li>-->
		          <li class="media <?php 
		                          if ($isGallery)  
		                          { 
		                          echo " current";
		                          }?>"><a href="<?php bloginfo('url'); ?>/gallery" class="media">Media Gallery</a><span class="sep"> / </span></li>
		                       <!--   <li><span class="sep">/</span></li>-->
		          <li class="community <?php 
		                          if ($isCommunity)  
		                          { 
		                          echo " current";
		                          }?>"><a href="<?php bloginfo('url'); ?>/community" class="community">Community</a><span class="sep"> / </span></li>
		                       <!--   <li><span class="sep">/</span></li>-->
		          <li class="contact <?php 
		                          if ($isContact)  
		                          { 
		                          echo " current";
		                          }?>"><a href="<?php bloginfo('url'); ?>/contact" class="contact">Contact</a></li>
		      </ul>
		      
		      <div class="contact-info">267 ELIZABETH STREET, NYC / 917-727-0179 / info@tacombi.com</div>
		</div>
		
</body>
</html>
