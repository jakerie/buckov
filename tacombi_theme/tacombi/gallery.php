<?php
/*
* @package WordPress
* @subpackage Tacombi_Theme
* Template Name: GalleryPage
*/

get_header(); ?>

<div id="tacombi-home">
	<div id="intro">
		<p> 
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<?php the_content(); ?>
		
		<?php endwhile; endif; ?>	
		</p>	</div>
	
	<?php include (TEMPLATEPATH . '/third-column.php'); ?>
	
	<div id="content" class="widecolumn" role="main">
	
	<div class="gallery-title">Tacombi</div>
    <?php echo do_shortcode('[flickrpress type="rss" url="http://api.flickr.com/services/feeds/photos_public.gne?id=49568096@N08&tags=tacombi&lang=en-us&format=rss_200" account="tacombi" tags="tacombi" count="6" paging="true" columns="6" view="squares" lightbox="true"]'); ?>
	
	<div class="gallery-title">Out &amp; About</div>
	<?php echo do_shortcode('[flickrpress type="rss" url="http://api.flickr.com/services/feeds/photos_public.gne?id=49568096@N08&tags=outandabout&lang=en-us&format=rss_200" account="tacombi" tags="tacombi" count="6" paging="true" columns="6" view="squares" lightbox="true"]'); ?>
	
	<div class="gallery-title">Aaron Sanchez</div>
	<?php echo do_shortcode('[flickrpress type="rss" url="http://api.flickr.com/services/feeds/photos_public.gne?id=49568096@N08&tags=aaronsanchez&lang=en-us&format=rss_200" account="tacombi" tags="tacombi" count="6" paging="true" columns="6" view="squares" lightbox="true"]'); ?>
	
	<div class="gallery-title">Fonda Nolita</div>
	<?php echo do_shortcode('[flickrpress type="rss" url="http://api.flickr.com/services/feeds/photos_public.gne?id=49568096@N08&tags=fondanolita&lang=en-us&format=rss_200" account="tacombi" tags="tacombi" count="6" paging="true" columns="6" view="squares" lightbox="true"]'); ?>
	
	</div>
	</div>
</div>


<?php get_footer(); ?>
