<?php
/*
* @package WordPress
* @subpackage Tacombi_Theme
* Template Name: CommunityPage
*/

get_header(); ?>

<div id="tacombi-home" class="community">
	<div id="intro">
		<p> 
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<?php the_content(); ?>
		
		<?php endwhile; endif; ?>	
		</p>	
	</div>
	
	<?php include (TEMPLATEPATH . '/third-column.php'); ?>
	
	<!--
	
	<?php
	
	$sidebar = get_post_meta($post->ID, "sidebar", true);
	
	get_sidebar($sidebar);
	?>
	
	-->
	
	<div id="content" class="widecolumn community" role="main">
<p class="upcoming-event-header">Upcoming Events:</p>
<?php echo do_shortcode('[google-calendar-events id="1" type="list-grouped" title=""]'); ?>
<div class="calendarlink"><a href="http://www.google.com/calendar/embed?src=fondanolita@gmail.com" class="thickbox lbpModal">*VIEW CALENDAR*</a>

</div>
<div style="clear:both;"> </div>

<div class="sep">
<div class="gallery-title">Out &amp; About</div>
<?php echo do_shortcode('[flickrpress type="rss" url="http://api.flickr.com/services/feeds/photos_public.gne?id=49568096@N08&tags=outandabout&lang=en-us&format=rss_200" account="tacombi" tags="tacombi" count="6" paging="true" columns="6" view="squares" lightbox="true"]'); ?>

<div style="clear:both;"> </div>
</div>


   <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
   <?php query_posts('&cat=10&showposts=1&paged=' .$paged); ?>
  <?php if (have_posts()) : ?>
<div class="sep">
		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<small>by <?php the_author() ?> on <?php the_time('j F Y') ?> | Comments[<?php comments_popup_link('0', '1', '%'); ?>]</small>

				<div class="entry">
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div>
<!--
				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
-->
 <div style="clear:both;"> </div>
 </div>
			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div>

</div>


<?php get_footer(); ?>
