<?php
/*
* @package WordPress
* @subpackage Tacombi_Theme
* Template Name: DrinksPage
*/

get_header(); ?>

<div id="tacombi-home">
	<div id="intro">
		<p> 
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<?php the_content(); ?>
		
		<?php endwhile; endif; ?>	
		</p>	</div>
	
	<?php include (TEMPLATEPATH . '/third-column.php'); ?>
	
	<div id="content" class="widecolumn" role="main">

   <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<?php query_posts('&cat=44&showposts=5'); ?>


	<?php if (have_posts()) : ?>
	<div class="list-menu">
		<span class="label-one"><a href="http://www.tacombi.com/food/drinks">Drinks</a></span>
		<span class="label-two">Bebidas</span>		
		<?php while (have_posts()) : the_post(); ?>
        <div class="menu-item">
                <span class="menu-item-name"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></span>
                <div class="menu-item-price"><?php $key="price"; echo get_post_meta($post->ID, $key, true); ?></div>
    		    <div class="menu-item-description"><?php echo the_excerpt(); ?></div>
    	</div>
<?php endwhile; ?>
    <div style="clear:both;"> </div>
</div>

	<?php else : ?>
<!--
		<h2 class="center">LOOKING FOR SOMETHING NOT HERE?</h2>
		<p class="center">We're new to the neighborhood and are constantly updating. Please check back soon.</p>
		<?php get_search_form(); ?>
-->
	<?php endif; ?>

<?php query_posts('&cat=44&showposts=5&paged=' .$paged); ?>


	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
		<div class="grid-view">
			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
              <div class="thumb">
                <?php
			    if ( has_post_thumbnail() ) {
			        the_post_thumbnail('thumbnail');
			    } else {
			        ?> 
			        <img src="http://www.tacombi.com/wp-content/themes/tacombi/images/nothumb.gif" width="175" alt="No Thumbnail Available" title="No Thumbnail Available"/>
			        <?php }
			    ?>
			    </div>
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			<!--	<small>by <?php the_author() ?> on <?php the_time('j F Y') ?> | Comments[<?php comments_popup_link('0', '1', '%'); ?>]</small> -->

				<div class="entry">
					<!--
								<?php the_content(); ?>
								-->
							</div>
							<div class="entry-comment">
							
							<?php 
							
							$recent_comments = get_comments( array(
							'post_id'	=> $post->ID,
							'number'    => 1,
							'status'    => 'approve'
							) );
							
							if ($recent_comments)
							{ 
							foreach($recent_comments as $comm) :
							  echo($comm->comment_content);
							endforeach;
							} 
							else
							{ 
							echo the_excerpt(); }
							?>
							
							
				<small>Comments[<?php comments_popup_link('0', '1', '%'); ?>]</small>
							
											</div>
<!--
				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
-->
			</div>
        </div>
		<?php endwhile; ?>
<div class="navigation">
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
</div>
	<?php else : ?>

		<h2 class="center">LOOKING FOR SOMETHING NOT HERE?</h2>
		<p class="center">We are constantly updating, check back again soon.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div>

</div>


<?php get_footer(); ?>
