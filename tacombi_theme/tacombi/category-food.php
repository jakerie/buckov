<?php
/*
Template Name: Food
*/

get_header(); ?>

<div id="tacombi-home">
	<!--<div id="intro">
		<p>We're sort of new to town...so bare with us as we add new content. In the meantime, come by for some tacos! Fonda Nolita is now open for breakfast and lunch from 8am-3pm.</p>
	</div>
	-->
	
	
	<?php include (TEMPLATEPATH . '/third-column.php'); ?>

	
	<div id="content" class="widecolumn" role="main">
	<!--
	<div class="menu">
		<div class="menu-title">Breakfast
			<br/>Desayuno
		</div>
		
		<div class="menu-item">
			<span class="menu-item-name">Breakfast Taco</span>
			<span class="menu-item-price">7.95</span>
		<span class="menu-item-description">A short description of the ingredients found in a Breakfast Taco</span>
			<div style="clear:both;"> </div>
		</div>
		
		<div class="menu-item">
			<span class="menu-item-name">Breakfast Taco</span>
			<span class="menu-item-price">7.95</span>
		
	<span class="menu-item-description">A short description of the ingredients found in a Breakfast Taco</span>
			<div style="clear:both;"> </div>
		</div>
		
		<div class="menu-item">
			<span class="menu-item-name">Breakfast Taco</span>
			<span class="menu-item-price">7.95</span>
	<span class="menu-item-description">A short description of the ingredients found in a Breakfast Taco</span>
			<div style="clear:both;"> </div>
		</div>
		
		<div class="menu-item">
			<span class="menu-item-name">Breakfast Taco</span>
			<span class="menu-item-price">7.95</span>
	<span class="menu-item-description">A short description of the ingredients found in a Breakfast Taco</span>
			<div style="clear:both;"> </div>
		</div>
	
	</div>
-->
	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
		<div class="grid-view">
			
			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			<!--	<small>by <?php the_author() ?> on <?php the_time('j F Y') ?> | Comments[<?php comments_popup_link('0', '1', '%'); ?>]</small>-->

				<div class="entry">
					<!--
					<?php the_content(); ?>
					-->
				</div>
				<div class="entry-comment">
				
				<?php 
				
				$recent_comments = get_comments( array(
				'post_id'	=> $post->ID,
				'number'    => 1,
				'status'    => 'approve'
				) );
				
				if ($recent_comments)
				{ 
				foreach($recent_comments as $comm) :
				  echo($comm->comment_content);
				endforeach;
				} 
				else
				{ 
				echo the_excerpt(); }
				?>
				
				
		
				
				
				</div>
				<p class="postmetadata">Comments[<?php comments_popup_link('0', '1', '%'); ?>]</p>
<!--			
				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
-->
			</div>
			
		</div>
		<?php endwhile; ?>

<div class="navigation">
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
</div>
	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div>
</div>


<?php get_footer(); ?>
