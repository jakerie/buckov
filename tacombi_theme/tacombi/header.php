<?php
/**
 * @package WordPress
 * @subpackage Tacombi_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/tacombie.css" media="screen" />
<![endif]-->
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<script type="text/javascript" src="<?php bloginfo('url');?>/wp-content/themes/tacombi/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('url');?>/wp-content/themes/tacombi/js/jquery.tools.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('url');?>/wp-content/themes/tacombi/js/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="<?php bloginfo('url');?>/wp-content/themes/tacombi/js/tacombi.js"></script>

<style type="text/css" media="screen">

<?php
// Checks to see whether it needs a sidebar or not
if ( empty($withcomments) && !is_single() ) {
?>

<?php } 
?>

</style>

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

<?php 
//these vars control nav logic / current / display
$isAbout = is_Page('About') || is_Page('Aaron Sanchez'); 
$isFood = is_Page('FoodPage') || is_Page('Food') || is_category('Food') || is_category('Breakfast') || is_category('Lunch') || is_category('Dinner') || is_category('Drinks') || is_Page('Breakfast') || is_Page('Lunch') || is_Page('Dinner') || is_Page('Drinks');
$isGallery = is_category('Gallery') || is_Page('GalleryPage') || is_Page('Gallery');
$isCommunity = is_category('Community') || is_Page('Community');
$isContact = is_Page('Contact');

?>

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page"<?php if (!is_front_page()) { echo " class=\"shortPage\"";}?>>


<div id="page-header"<?php if (!is_front_page()) { echo " class=\"shortPage\"";}?>>
    <div id="slider"<?php if (!is_front_page()) { echo " class=\"shortSlider\"";}?>>
    	<div id="mask-gallery"<?php if (!is_front_page()) { echo " class=\"shortMask\"";}?>>
    <?php if (!is_front_page()) : ?>
    <ul id="gallery" class="shortGallery">
    	<li><img src="<?php bloginfo('url');?>/wp-content/themes/tacombi/images/subheader1.jpg" width="990" height="307" alt=""/></li>
    	<li><img src="<?php bloginfo('url');?>/wp-content/themes/tacombi/images/subheader2.jpg" width="990" height="307" alt=""/></li>
    	<li><img src="<?php bloginfo('url');?>/wp-content/themes/tacombi/images/subheader3.jpg" width="990" height="307" alt=""/></li>
    </ul>
    <?php else : ?>
	<ul id="gallery" class="full">
		<li><img src="<?php bloginfo('url');?>/wp-content/themes/tacombi/images/header1v2.jpg" width="990" height="474" alt=""/></li>
		<li><img src="<?php bloginfo('url');?>/wp-content/themes/tacombi/images/header2v2.jpg" width="990" height="474" alt=""/></li>
		<li><img src="<?php bloginfo('url');?>/wp-content/themes/tacombi/images/header3v2.jpg" width="990" height="474" alt=""/></li>
	</ul>
    <?php endif; ?>
    	</div>
    	
    	<div id="mask-excerpt">
    	<ul id="excerpt">
    	<!--	<li>Hello</li>-->
    	</ul>
    	</div>
    	
    	<div class="social-icons">
    	<ul>
    	        		<li id="social-icon-spread"><img src="<?php bloginfo('url'); ?>/wp-content/themes/tacombi/images/social-icons.png" border="0" usemap="#social"/><map id="social" name="social"><area shape="rect" alt="Twitter" title="" coords="1,1,50,16" href="http://twitter.com/tacombi" target="_blank" /><area shape="rect" alt="Facebook" title="" coords="56,1,113,16" href="http://www.facebook.com/tacombinyc" target="_blank" /><area shape="rect" alt="Flickr" title="" coords="120,0,155,15" href="http://www.flickr.com/photos/tacombi" target="_blank" /><area shape="rect" alt="YouTube" title="" coords="169,2,210,17" href="http://www.youtube.com/results?search_query=TACOMBI" target="_blank" /><area shape="rect" alt="Vimeo" title="" coords="221,3,261,13" href="http://www.vimeo.com/tacombi" target="_blank" /></map></li>
        </ul>
        </div>
        <div style="clear:both;"> </div>

    	<div id="google-cal-large">
    	   <?php echo do_shortcode('[google-calendar-events id="1" type="ajax"]'); ?>
    	   <span class="cal-link"><a class="thickbox lbpModal cboxElement" href="http://www.google.com/calendar/embed?src=fondanolita@gmail.com">EVENT CALENDAR</a></span>
    	</div>

        <div style="clear:both;"> </div>
        
        <div id="header-search">
            <form role="search" method="get" id="searchform" action="<?php bloginfo('url'); ?>">
                <div><label class="screen-reader-text" for="s">Search for:</label>
                    <input type="text" value="DONDE ESTAS" name="s" id="hs" />
                    <input type="image" src="<?php bloginfo('url');?>/wp-content/themes/tacombi/images/sunglass-submit.gif" id="searchsubmit" value="Search" />
                </div>
            </form>
        </div>
    
    </div>
    
    <div id="taco-nav">
    	<ul class="nav">
    		<li id="banner-header"><span> </span></li>
    		<li<?php 
    		                if ($isAbout)  
    		                { 
    		                echo " class=\"current\"";
    		                }?>><a href="<?php bloginfo('url'); ?>/about" class="about"><span> </span></a></li>
    		<li<?php 
    		                if ($isFood)  
    		                { 
    		                echo " class=\"current\"";
    		                }?>><a href="<?php bloginfo('url'); ?>/food" class="food"><span> </span></a></li>
    		<li<?php 
    		                if ($isGallery)  
    		                { 
    		                echo " class=\"current\"";
    		                }?>><a href="<?php bloginfo('url'); ?>/gallery" class="media"><span> </span></a></li>
    		<li id="join-us"<?php 
    		                if ($isCommunity)  
    		                { 
    		                echo " class=\"current\"";
    		                }?>><a href="<?php bloginfo('url'); ?>/community" class="community"><span> </span></a></li>
    	</ul>
    </div>
    
    <div class="clear:both;"> </div>
    
    <div class="taco-subnav">
  
        <ul class="nav">
            <li id="logo">
            <a href="<?php bloginfo('url'); ?>" class="home"><span class="logoImg"> </span></a>
             </li>
              <li class="about <?php 
                              if ($isAbout)  
                              { 
                              echo " current";
                              }?>"><a href="<?php bloginfo('url'); ?>/about" class="about">About</a><span class="sep"> / </span></li>
                          <!--   <li><span class="sep">/</span></li>-->
            <li class="food <?php 
                            if ($isFood)  
                            { 
                            echo " current";
                            }?>"><a href="<?php bloginfo('url'); ?>/food" class="food">Food</a><span class="sep"> / </span></li>
                           <!--   <li><span class="sep">/</span></li>-->
            <li class="media <?php 
                            if ($isGallery)  
                            { 
                            echo " current";
                            }?>"><a href="<?php bloginfo('url'); ?>/gallery" class="media">Media Gallery</a><span class="sep"> / </span></li>
                         <!--   <li><span class="sep">/</span></li>-->
            <li class="community <?php 
                            if ($isCommunity)  
                            { 
                            echo " current";
                            }?>"><a href="<?php bloginfo('url'); ?>/community" class="community">Community</a><span class="sep"> / </span></li>
                         <!--   <li><span class="sep">/</span></li>-->
            <li class="contact <?php 
                            if ($isContact)  
                            { 
                            echo " current";
                            }?>"><a href="<?php bloginfo('url'); ?>/contact" class="contact">Contact</a></li>
        </ul>        
        <?php if($isAbout || $isFood || $isCommunity) { ?>
        <ul class="context-nav">
        	<?php if($isAbout) { ?>
        		<li class="onceupon<?php 
        		                if (is_Page('About'))  
        		                { 
        		                echo " current";
        		                }?>"><a href="<?php bloginfo('url'); ?>/about">Once Upon a Time</a><span class="sep"> / </span></li>
        		<li class="chef<?php 
        		                if (is_Page('Aaron Sanchez'))  
        		                { 
        		                echo " current";
        		                }?>"><a href="<?php bloginfo('url'); ?>/aaron-sanchez">Aaron Sanchez</a></li>
        		<!--<li class="Partnerships">Partnerships</li>-->
        	<? }?>
		<?php if($isFood) { ?>
			<li class="breakfast<?php 
			                if (is_category('Breakfast') || is_Page('Breakfast'))  
			                { 
			                echo " current";
			                }?>"><a href="<?php bloginfo('url'); ?>/food/breakfast">Breakfast</a><span class="sep"> / </span></li>
			<li class="lunch<?php 
			                if (is_category('Lunch') || is_Page('Lunch'))  
			                { 
			                echo " current";
			                }?>"><a href="<?php bloginfo('url'); ?>/food/lunch">Lunch</a><span class="sep"> / </span></li>
			<li class="dinner<?php 
			                if (is_category('Dinner') || is_Page('Dinner'))  
			                { 
			                echo " current";
			                }?>"><a href="<?php bloginfo('url'); ?>/food/dinner">Dinner</a><span class="sep"> / </span></li>
			<li class="drinks<?php 
			                if (is_category('Drinks') || is_Page('Drinks'))  
			                {
			                echo " current";
			                }?>"><a href="<?php bloginfo('url'); ?>/food/drinks">Drinks</a></li>
		<? }?>
		<?php if($isCommunity) { ?>
			<li class="calendar current"><a href="<?php bloginfo('url'); ?>/community" class="">Calendar</a></li>
			<!--<li class="Partnerships">Partnerships</li>-->
		<? }?>
        	
        		
        	
        	
        	
       	</ul>
        
        
        <? }?>
        
    </div>
</div>

<hr />
