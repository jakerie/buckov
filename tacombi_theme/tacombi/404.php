<?php
/**
 * @package WordPress
 * @subpackage Tacombi_Theme
 */

get_header();
?>

	<div id="content" class="narrowcolumn">

		<h2 class="center">LOOKING FOR SOMETHING NOT HERE?</h2>
		<p class="center">We're new to the neighborhood and are constantly updating. Please check back soon.</p>
		<?php get_search_form(); ?>

	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>