<?php
/*
* @package WordPress
* @subpackage Tacombi_Theme
* Template Name: Chef
*/

get_header(); ?>

<div id="tacombi-home">
	<div id="intro">
		<p> 
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<?php the_content(); ?>
		
		<?php endwhile; endif; ?>	
		</p>	</div>
	
	<?php include (TEMPLATEPATH . '/third-column.php'); ?>
	
	<div id="content" class="widecolumn" role="main">

 <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;?>

<?php query_posts('cat=5&paged=' . $paged); ?>


	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<small>by <?php the_author() ?> on <?php the_time('j F Y') ?> | Comments[<?php comments_popup_link('0', '1', '%'); ?>]</small>

				<div class="entry">
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div>
<!--
				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
-->
			</div>

		<?php endwhile; ?>



<div class="navigation">
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
</div>



	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div>

</div>


<?php get_footer(); ?>
