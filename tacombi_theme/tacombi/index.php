<?php
/**
 * @package WordPress
 * @subpackage Tacombi_Theme
 */

get_header(); ?>

<div id="tacombi-home">
	<div id="intro">
		<p><strong>Now serving breakfast, lunch, and dinner from 8:00am-2:00am. </strong><br> 
		Peace, Love, and Tacos. From the beaches of Playa del Carmen to the streets of NYC, comes Tacombi parked at 267 Elizabeth Street at Fonda Nolita.
		</p>
	</div>
	
	<?php include (TEMPLATEPATH . '/third-column.php'); ?>
	
	<div id="content" class="narrowcolumn" role="main">
 <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;?>

<?php query_posts('showposts=5&cat=-11,-7,-9,-44,-8&paged=' . $paged); ?>
	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<small>by <?php the_author() ?> on <?php the_time('j F Y') ?> | Comments[<?php comments_popup_link('0', '1', '%'); ?>]</small>

				<div class="entry">
					<?php the_content('*MORE*'); ?>
				</div>
<!--
				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
-->
			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div>
	
<?php get_sidebar(); ?>

<div style="clear:both;"> </div>

</div>


<?php get_footer(); ?>
