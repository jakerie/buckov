<?php
/**
 * @package WordPress
 * @subpackage Tacombi_Theme
 */

get_header(); ?>
<div id="tacombi-home">
	<div id="intro">
		<p>		
	<?php if (have_posts()) : ?>

 	  <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
 	  <?php /* If this is a category archive */ if (is_category()) { ?>
		Archive for the &#8216;<span class="searchResult"><?php single_cat_title(); ?></span>&#8217; Categoryp
 	  <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
		Posts Tagged &#8216;<span class="searchResult"><?php single_tag_title(); ?></span>&#8217;
 	  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		Archive for <span class="searchResult"><?php the_time('F jS, Y'); ?></span>
 	  <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		Archive for <span class="searchResult"><?php the_time('F, Y'); ?></span>
 	  <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		prchive for <span class="searchResult"><?php the_time('Y'); ?></span>
	  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
		Author Archive
 	  <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		Blog Archives
 	  <?php } ?>
 	  
		</p>	
	</div>
	
		<?php include (TEMPLATEPATH . '/third-column.php'); ?>

	
	<div id="content" class="widecolumn searchPage" role="main">

		<?php while (have_posts()) : the_post(); ?>
			<div <?php post_class() ?>>
						<div class="archive_list"><span id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a> </span> by <span class="author"><?php the_author() ?></span> on <?php the_time('j F Y') ?>
					</div>
    </div>
					
		<?php endwhile; ?>

<div class="navigation">
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
</div>
	<?php else :

		if ( is_category() ) { // If this is a category archive
			printf("<h2 class='center'>Sorry, but there aren't any posts in the %s category yet.</h2>", single_cat_title('',false));
		} else if ( is_date() ) { // If this is a date archive
			echo("<h2>Sorry, but there aren't any posts with this date.</h2>");
		} else if ( is_author() ) { // If this is a category archive
			$userdata = get_userdatabylogin(get_query_var('author_name'));
			printf("<h2 class='center'>Sorry, but there aren't any posts by %s yet.</h2>", $userdata->display_name);
		} else {
			echo("<h2 class='center'>No posts found.</h2>");
		}
		get_search_form();

	endif;
?>

	</div>
	</div>


<?php get_footer(); ?>
