$(document).ready(function() {

	$('.DaikosText .comments').click(function() {

		$('.comments-content').removeClass("hidden");
		$('.DaikosText > span').removeClass("active");
		$('.comments').addClass("active");
		$('.posts-content').addClass("hidden");
		$('.tags-content').addClass("hidden");
	});

	$('.DaikosText .posts').click(function() {

		$('.posts-content').removeClass("hidden");
		$('.DaikosText > span').removeClass("active");
		$('.posts').addClass("active");
		$('.comments-content').addClass("hidden");
		$('.tags-content').addClass("hidden");
	});
	
	
	$('.DaikosText .tags').click(function() {

		$('.tags-content').removeClass("hidden");
		$('.DaikosText > span').removeClass("active");
		$('.tags').addClass("active");
		$('.posts-content').addClass("hidden");
		$('.comments-content').addClass("hidden");
	});
	
	$('#header-search #hs').focusin(function() {
	    if ($('input#hs').val() == 'DONDE ESTAS') {
	        $('input#hs').val('');
	    }
	});
	
	$('#header-search #hs').focusout(function() {
	    if ($('input#hs').val() == '') {
	        $('input#hs').val('DONDE ESTAS');
	    }
	});
	
	$('#author').focusin(function() {
	    if (($('#author').val() == '[NAME] Required') || ($('#author').val() == '[NAME]')) {
	        $('#author').val('');
	    }
	});
	
	$('#email').focusin(function() {
	    if (($('#email').val() == '[E-MAIL] Required') || ($('#email').val() == '[E-MAIL]')) {
	        $('#email').val('');
	    }
	});
	
	$('#url').focusin(function() {
	    if (($('#url').val() == '[WEBSITE] Required') || ($('#url').val() == '[WEBSITE]')) {
	        $('#url').val('');
	    }
	});
    	    
    $('#comment').focusin(function() {
        if ($('#comment').val() == '[COMMENT]') {
            $('#comment').val('');
        }
    });
    
    $('.spreadtheword').tooltip({effect: 'slide', position: 'bottom center'});
	
	


	//Speed of the slideshow
	var speed = 10000;
	
	//You have to specify width and height in #slider CSS properties
	//After that, the following script will set the width and height accordingly
	$('#mask-gallery, #gallery li').width($('#slider').width());	
	$('#gallery').width($('#slider').width() * $('#gallery li').length);
	$('#mask-gallery, #gallery li, #mask-excerpt, #excerpt li').height($('#slider').height());
	
	//Assign a timer, so it will run periodically
	var run = setInterval('newsslider(0)', speed);	
	
	$('#gallery li:first, #excerpt li:first').addClass('selected');

	//Pause the slidershow with clearInterval
	$('#btn-pause').click(function () {
		clearInterval(run);
		return false;
	});

	//Continue the slideshow with setInterval
	$('#btn-play').click(function () {
		run = setInterval('newsslider(0)', speed);	
		return false;
	});
	
	//Next Slide by calling the function
	$('#btn-next').click(function () {
		newsslider(0);	
		return false;
	});	

	//Previous slide by passing prev=1
	$('#btn-prev').click(function () {
		newsslider(1);	
		return false;
	});	
	
	//Mouse over, pause it, on mouse out, resume the slider show
	$('#slider').hover(
	
		function() {
			clearInterval(run);
		}, 
		function() {
			run = setInterval('newsslider(0)', speed);	
		}
	); 	
		
	
	
});


function newsslider(prev) {

	//Get the current selected item (with selected class), if none was found, get the first item
	var current_image = $('#gallery li.selected').length ? $('#gallery li.selected') : $('#gallery li:first');
	var current_excerpt = $('#excerpt li.selected').length ? $('#excerpt li.selected') : $('#excerpt li:first');

	//if prev is set to 1 (previous item)
	if (prev) {
		
		//Get previous sibling
		var next_image = (current_image.prev().length) ? current_image.prev() : $('#gallery li:last');
		var next_excerpt = (current_excerpt.prev().length) ? current_excerpt.prev() : $('#excerpt li:last');
	
	//if prev is set to 0 (next item)
	} else {
		
		//Get next sibling
		var next_image = (current_image.next().length) ? current_image.next() : $('#gallery li:first');
		var next_excerpt = (current_excerpt.next().length) ? current_excerpt.next() : $('#excerpt li:first');
	}

	//clear the selected class
	$('#excerpt li, #gallery li').removeClass('selected');
	
	//reassign the selected class to current items
	next_image.addClass('selected');
	next_excerpt.addClass('selected');

	//Scroll the items
	$('#mask-gallery').scrollTo(next_image, 800);		
	//$('#mask-excerpt').scrollTo(next_excerpt, 800);					
	
}
