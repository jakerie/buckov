<?php
/*
Template Name: Contact
*/

get_header(); ?>

	
<div id="tacombi-home">
	<div id="intro">
	<p> 
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<?php the_content(); ?>
	
	<?php endwhile; endif; ?>	
	</p>
	</div>
		
	<?php include (TEMPLATEPATH . '/third-column.php'); ?>
			
	<div id="content" class="narrowcolumn" role="main">
	
	<div class="address">
		<p class="address">
		Fonda Nolita
		<br/>267 Elizabeth Street
		<br/>917-727-0179
		</p>
	</div>

	<div class="map">
				<img width="563" height="475" alt="Tacombi!" src="http://maps.google.com/maps/api/staticmap?center=267+Elizabeth+Street,New+York,NY&zoom=18&size=563x475&maptype=roadmap
		&markers=icon:http://bit.ly/ahTiB8|40.724046,-73.993875&sensor=false"/>
		
	</div>
	
	<div class="contact">
	<p class="contact-header">Contact Form</p>
	<?php echo do_shortcode( '[contact-form 1 "Contact form 1"]' ); ?>
	</div>
	

	</div>

</div>


<?php get_footer(); ?>
