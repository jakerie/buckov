(function($) {

    $(document).ready(function() {
        
        userData.init();
        cartActions.animate('minicart');
        cartActions.retrieveCart();
        //setup modal for acts
        $('a.act-info').bind('click', function(e) {
            e.preventDefault();
            var type=$(this).attr('data-controls-type');
            gameData.setOverlayContent(type);          
        });

        $('.updatecoins', '.topbar').bind('click', function(e) {
            userData.incrementCoins();
        });

       // $('#minicart').cartUpdate('updater');

    });

    var userData=function() {
        
        var  localConfig = {};

            var setConfig = function(data) {          
                $.extend(true, localConfig, data); 
                updateCoins(); 
            };
            
            var getConfig = function() {
                return localConfig;
            };

            var init = function() {
                //only if there are cookie values not available for processing
                getData('app');
            };
       
            var getData = function(type) {
               var jsonFile = type=='app'?'json/app.json':'json/game.json';
        
              $.ajax({
                  type: "GET",
                  url: jsonFile,
                  processData: true,
                  data: {},
                  dataType: "json",
                  success: function(json) {
                       setConfig(json);
                  },
                  error: function(x,y,z) {
                      //console.log(x.responseText);
                 }
              });
  
           };
       
           var incrementCoins=function(){
               var val = localConfig.user.denier;
               setConfig({"user":{"denier":val+1}});
           }
       
           var updateCoins=function(val){
               if (val !== undefined) {
                  setConfig({"user":{"denier":val}});
               } 
               
               $("#denierCount").html('<span>'+localConfig.user.denier+'</span>');               
               $(".wallet strong").html(localConfig.user.denier);
               
 
            };
       
       return {
           "init" : init,
           "setConfig": setConfig,
           "getConfig": getConfig,
           "incrementCoins":incrementCoins
        }
    }();
    
    var cartActions=function() {
        var open=false;
        var cartState={};
        
        var addItem = function(type, addedGameData) {
            //only one item for now?
            
            //retrieveCart();
            
            $.extend(true, cartState, {type:addedGameData});
            updateCarts(addedGameData);
            
         }
         
         var updateCarts = function(addedGameData) {
             var gameData = addedGameData||cartState.type;
             $('#minicart-items').cartUpdate('updater', gameData);
             $('#overlaycart-items').cartUpdate('updater', gameData);
             letsDoCookies.setThis("bovCartState", cartState);
             if ($('#maincart-items').get(0) !== null) {
                 $('#maincart-items').cartUpdate('updater', gameData);
                  //this obviously doesn't belong here
                 $.each(gameData.Scenes, function(key, value) {
                     $('.scenes', '#maincart-items').append('<em>Scene '+key+'</em>: '+value+' ');
                 });
             }
        }
         
        var retrieveCart = function() {
            var cookieExists = letsDoCookies.getThis('bovCartState'),
                cartFromCookie;
            if (cookieExists !== null) {
                if(cookieExists !== undefined) {
                var cartFromCookie = $.parseJSON(cookieExists);
                cartState = cartFromCookie; 
                updateCarts();
                 }
            }      
        }
        var animate = function(sel) {
            var triggerBtn = '#'+sel+'Btn';
            var content = '#'+sel;
            $(triggerBtn).click(function() {
                if(open===false){
                    $(content).slideUp();
                    open=true;
                } else {
                    $(content).slideDown();
                    open=false;
                }
            });
        };
        return {
            "animate": animate,
            "addItem": addItem,
            "retrieveCart": retrieveCart
        }
    }();
    
    var gameData=function() {
        var localConfig={};
            
            var setConfig = function(data) {      
                //console.log(data);       
                $.extend(true, localConfig, data);  
            };
            
            var getConfig = function() {
                return localConfig;
            };
          
        
            var templates = function(type) {
                var tmpl;
                
                if (type=='empty') {
                    tmpl="<div class='modal-header'><a href='#' class='close'>x</a><h4>Loading…</h4></div><div class='modal-body'>" +
                         "<p class='price' style='display:none;'><em>Cost:</em> 2 Deniers</p>" +
                         "<p>Loading…</p></div><div class='modal-footer' style='display:none;'><a class='btn primary' href='#'>I want to play</a>" +
                         "<a class='btn secondary' href='#'>Maybe not...sounds scary</a></div>";   
                } else if (type=='purchase') {
                    tmpl="<div class='modal-header'><a href='#' class='close'>x</a><h4>${name}</h4></div><div class='modal-body'><div class='alert-wrapper'></div>" +
                         "<p class='price'><em>Cost:</em> ${price} Deniers</p>" +
                         "<p>${shortSummary}</p></div><div class='modal-footer'><a class='btn primary' href='#'>I want to play</a>" +
                         "<a class='btn secondary' href='#'>Maybe not...sounds scary</a></div>";
                }
                return tmpl;
            }
            
            var setOverlayContent = function(type) {
                
                if ($.isEmptyObject(localConfig)) {
                    //getData will send back to setOverlayContent when called from here
                                        
                    $("#act-modal").html('');
                    $.tmpl(templates('empty'), {}).appendTo("#act-modal");
                    getData('setContent', type);
                } else {
                    $("#act-modal").html('');
                    $.tmpl(templates('purchase'), localConfig[type] ).appendTo("#act-modal"); 
                    attachActions(type);                  
                }
                
            };
            
            var attachActions = function(type) {
                $('a.secondary', '#act-modal').bind('click', function(e) {
                    e.preventDefault();
                    $('#act-modal').modal('hide');
                  });
              
                $('a.primary', '#act-modal').bind('click', function(e) {
                    e.preventDefault();
                    checkWallet(type);
                });
            };
            
            var checkWallet = function(type) { 
                var config = userData.getConfig(),
                    currWallet = config.user.denier;
 
                    if (currWallet >= localConfig[type].price) {
                        //for purchase action
                        //userData.updateCoins(currWallet-localConfig[type].price);
                        //add item to cart object
                        cartActions.addItem(type, localConfig[type]);
                        $('#act-modal').modal('hide');
                    } else {
                       $('.alert-wrapper', '#act-modal').html(''); 
                       $('.alert-wrapper', '#act-modal').prepend("<div class='alert-message error fade in' data-alert='alert'>Oh boy, it looks like you can't afford that with only " + currWallet + " deniers.</div>");
                    }
                                    
            }
            
            var getData = function(state, type) {
                var step = state,
                    jsonFile = 'json/game.json';

                $.ajax({
                    type: "GET",
                    url: jsonFile,
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function(json) {
                       setConfig(json);
                       if (step=='setContent'){
                           setOverlayContent(type);
                       }
                    },
                    error: function(x,y,z) {
                  
                    }
                });
            };
               
           return {
               "setOverlayContent":setOverlayContent
           }
    
    }();
    
    var letsDoCookies = function() {
        var setThis = function(key, obj) {
             var string=JSON.stringify(obj);
              $.cookie(key, string, {expires: 2});
        }
        
        var getThis = function(key){ 
            return $.cookie(key);
        }
        
        return {
            "setThis": setThis,
            "getThis":getThis
        }
    }();    
   

})(jQuery);