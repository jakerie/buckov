(function( $ ){

 var methods = {
     init : function() {

     },
     fadeIn : function() {
         this.fadeIn('normal', function(){
             //show cart on page load if it contains items
         });
     },
    templates: function(w) {
           var mini, modest, large;
          
           mini = "<li class='item'>${name}</li>";
           modest = "<li class='item'><div class='name'>${name}</div><div class='price'>Cost: <em>${price}</em></div><div class='scenes'></div><div class='shortSum'>${shortSummary}</div></li>";
           large = "<li class='item'><div class='img'><img src='${secondaryimage}' alt='${name}' /></div><div class='name'>${name}</div><div class='price'>Cost: <em>${price}</em></div><div class='scenes'></div><div class='longSum'><blockquote>${longSummary}</blockquote></div></li>";

                if (w < 150) {
                      return mini;            
                   } else if (w <= 520) {
                      return modest;
                   } else {
                      return large;
                   }
    },
    updater : function(model) {
         var tmpl = $.fn.cartUpdate('templates', this.width());
         this.html('');
         $.tmpl(tmpl, model).appendTo(this);                
    }
}

$.fn.cartUpdate = function(method) {

     if ( methods[method] ) {
       return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
     } else if ( typeof method === 'object' || ! method ) {
       return methods.init.apply( this, arguments );
     } else {
       $.error( 'Don\'t be stupid with that:  ' +  method + '!' );
     }    
   };

})( jQuery );