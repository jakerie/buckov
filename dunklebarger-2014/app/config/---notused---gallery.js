define(function( require, exports, module ) {
/*
 *
 * SITE CONFIG
 *
 */
 
return {
	"galleries": [
		{
			"title": "Custom Built",
			"images": [
				{
					"img":"/app/images/gallery/Georgie-007.jpg",
					"description":"Georgie",
					"alt":"Georgie"
				},
				{
					"img":"/app/images/gallery/img565.jpg",
					"description":"",
					"alt":"Shelf"
				},	
				{
					"img":"/app/images/gallery/photo06.jpg",
					"description":"Credenza",
					"alt":""
				}
			]
		},
		{
			"title": "Work",
			"images": [
				{
					"img":"/app/images/gallery/Georgie-007.jpg",
					"description":"Georgie",
					"alt":"Georgie"
				},
				{
					"img":"/app/images/gallery/img565.jpg",
					"description":"",
					"alt":"Shelf"
				},	
				{
					"img":"/app/images/gallery/photo06.jpg",
					"description":"Credenza",
					"alt":""
				}
			]
		}
	]
}

});
