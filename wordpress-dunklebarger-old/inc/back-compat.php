<?php
/**
 * Dunklebarger back compat functionality
 *
 * Prevents Dunklebarger from running on WordPress versions prior to 3.6,
 * since this theme is not meant to be backward compatible and relies on
 * many new functions and markup changes introduced in 3.6.
 *
 * @package Buckov
 * @subpackage Dunklebarger
 * @since Dunklebarger 1.0
 */

/**
 * Prevent switching to Dunklebarger on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Dunklebarger 1.0
 *
 * @return void
 */
function dunklebarger_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'dunklebarger_upgrade_notice' );
}
add_action( 'after_switch_theme', 'dunklebarger_switch_theme' );

/**
 * Add message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Dunklebarger on WordPress versions prior to 3.6.
 *
 * @since Dunklebarger 1.0
 *
 * @return void
 */
function dunklebarger_upgrade_notice() {
	$message = sprintf( __( 'Dunklebarger requires at least WordPress version 3.6. You are running version %s. Please upgrade and try again.', 'dunklebarger' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevent the Theme Customizer from being loaded on WordPress versions prior to 3.6.
 *
 * @since Dunklebarger 1.0
 *
 * @return void
 */
function dunklebarger_customize() {
	wp_die( sprintf( __( 'Dunklebarger requires at least WordPress version 3.6. You are running version %s. Please upgrade and try again.', 'dunklebarger' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'dunklebarger_customize' );

/**
 * Prevent the Theme Preview from being loaded on WordPress versions prior to 3.4.
 *
 * @since Dunklebarger 1.0
 *
 * @return void
 */
function dunklebarger_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Dunklebarger requires at least WordPress version 3.6. You are running version %s. Please upgrade and try again.', 'dunklebarger' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'dunklebarger_preview' );
